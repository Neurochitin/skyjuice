# Neurochitin's skyjuice

skyjuice is a tool to *accurately* and *reproducibly* rip the music from Pokémon Mystery Dungeon 2: Explorers of Sky.

## How does it work?

In essence, it runs the game in an emulator and plays through the entire Sky Jukebox, recording the sound output into a file. That's not really the entire story though: it doesn't just run the emulator program and uses system audio interfaces to record the output, like previous rips of the EoS soundtrack have done. **Instead, skyjuice integrates directly into the codebase of [melonDS](https://github.com/Arisotura/melonDS), a particularly accurate Nintendo DS emulator.** In this way it can control all aspects of the emulation itself, and record the raw audio data as it comes in, without having to go through other software which might introduce artifacts. The emulation is run without synchronising to an external timing source, which avoids synchronisation artifacts and lets the emulation run much faster than real-time. In addition, skyjuice is able to read the number and name of the currently played track directly from the emulator memory, allowing it to separate the recorded audio data into individual tracks at precisely the correct locations.

skyjuice will output the audio data as s16le PCM at first, however it also includes a post-processing script that converts the PCM files into correctly-tagged FLAC files.

## How to use it?

Running skyjuice on your own is possible, but not easy. (Mainly because I was too lazy to figure out how to properly use CMake for something like this) You first need the following prerequisites:

1. A Linux system (Other systems might also work but are untested)
2. A local cloned copy of melonDS, commit [`0761fe7`](https://github.com/Arisotura/melonDS/tree/0761fe736f1efb633c8c2b947f53176309961dbb), as well as all prerequisites required for building it
3. A Western ROM of PMD2: EoS
4. A `sav`-format save file for the game where the Sky Jukebox is unlocked. Ideally, it would also have all tracks unlocked (i.e. all bonus episodes completed); if this is not the case, skyjuice should also work but will only be able to rip the unlocked tracks. The language of the save file does not matter for the audio data, but the resulting files will have localised names and tags.

Once you have all these, follow the following steps:

1. Copy `skyjuice.cpp` from this repository to `melonDS/src/frontend/qt_sdl/skyjuice.cpp`.
2. Append the following lines to `melonDS/src/frontend/qt_sdl/CMakeLists.txt`:
```cmake
add_executable(skyjuice skyjuice.cpp Config.cpp LAN_PCap.cpp LAN_Socket.cpp ROMManager.cpp SaveManager.cpp Platform.cpp ArchiveUtil.h ArchiveUtil.cpp ../Util_Audio.cpp)
target_link_libraries(skyjuice ${CMAKE_THREAD_LIBS_INIT})
target_include_directories(skyjuice PRIVATE ${SDL2_INCLUDE_DIRS} ${SDL2_PREFIX}/include ${SLIRP_INCLUDE_DIRS} ${LIBARCHIVE_INCLUDE_DIRS})
target_link_directories(skyjuice PRIVATE ${SDL2_LIBRARY_DIRS} ${SLIRP_LIBRARY_DIRS})
target_link_directories(skyjuice PRIVATE ${LIBARCHIVE_LIBRARY_DIRS})
target_include_directories(skyjuice PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
target_include_directories(skyjuice PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/..")
target_include_directories(skyjuice PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/../..")
target_link_libraries(skyjuice ${SDL2_LIBRARIES} ${SLIRP_LIBRARIES} ${LIBARCHIVE_LIBRARIES})
target_link_libraries(skyjuice ${QT_LINK_LIBS})
target_link_libraries(skyjuice core)
```
3. Build melonDS according to its instructions. If you get an error message about `emuStop` in the linking step, you may have to comment out lines 67 and 123 of `melonDS/src/frontend/qt_sdl/Platform.cpp` and try again. If everything worked fine, you should have a `skyjuice` executable file in the output folder.
4. Copy the save file to the same path as your ROM, and make sure it is named equivalently to the ROM (so if the ROM is named `Pokemon_Mystery_Dungeon_Explorers_of_Sky.nds`, the save file should be named `Pokemon_Mystery_Dungeon_Explorers_of_Sky.sav`).
5. Run skyjuice. It is run like `./skyjuice path/to/rom.nds`; the first and only argument should be the path to the ROM file. Ideally this should rip one track after the other to a folder called `skyjuice_out` in the current directory. The program should give you status updates in the form of "new track" messages. The whole thing will take perhaps 45 to 90 minutes, depending on your CPU speed.
6. Once everything is done, run the post-processing script like `ruby postprocess.rb path/to/skyjuice_out path/to/output_folder`. The desired output folder will be created if it does not exist. Afterwards, the output folder should contain all 141 FLAC files, correctly tagged.
7. Enjoy the music!
