// skyjuice by Neurochitin
// Reproducibly rips music from the Sky Jukebox (from PMD2: EoS) using an
// accurate emulator (melonDS). See the README for usage instructions.

#include <iostream>
#include <fstream>
#include <filesystem>

#include "types.h"

#include "NDS.h"
#include "GPU.h"
#include "SPU.h"
#include "Platform.h"
#include "FrontendUtil.h"
#include "ROMManager.h"

#define SAMPLERATE 48000

#define KEYMASK_A 0xFFE
#define KEYMASK_UP 0xFBF
#define KEYMASK_DOWN 0xF7F

std::string outfolder = "skyjuice_out";
std::ofstream pcmfile;
bool writepcm = false;
int prevtracknum = 0;
bool lasttrack = false;
int framecount = 0;

void skyerr(std::string str) {
  std::cerr << " ~error: " << str << "~" << std::endl;
}

void skyprint(std::string str) {
  std::cout << " ~" << str << "~" << std::endl;
}

void skyinit() {
  const char *fakeargv[1] = { "skyjuice" };
  Platform::Init(1, (char**)fakeargv);

  NDS::Init();
  GPU::InitRenderer(0);

  GPU::RenderSettings videosettings;
  GPU::SetRenderSettings(0, videosettings);
  SPU::SetInterpolation(0);

  Frontend::Init_Audio(SAMPLERATE);

  skyprint("nds init done");
}

void cleanup() {
  GPU::DeInitRenderer();
  NDS::DeInit();
}

bool framebetween(int min, int max) {
  return framecount > min && framecount <= max;
}

bool navigate() {
  if (framecount == 20) skyprint("now navigating to the sky jukebox. this will take a few seconds");

  if (framebetween(1000, 1005) || // 1. skip opening animation
      framebetween(1200, 1205)) { // 2. skip title screen
    NDS::SetKeyMask(KEYMASK_A);
  }

  if (framebetween(1400, 1405) || // 3. move cursor to episode list on start screen
      framebetween(1450, 1455)) { // 4. move cursor to "other" on start screen
    NDS::SetKeyMask(KEYMASK_UP);
  }

  if (framebetween(1500, 1505)) { // 5. activate "other"
    NDS::SetKeyMask(KEYMASK_A);
  }

  if (framebetween(1700, 1705)) { // 6. move cursor to sky jukebox on "other" screen
    NDS::SetKeyMask(KEYMASK_DOWN);
  }

  if (framebetween(1900, 1905) || // 7. activate sky jukebox
      framecount == 2000) {       // 8. start playing first track
    NDS::SetKeyMask(KEYMASK_A);
  }

  if (framecount > 2050) {
    // at this point we should have reached it
    skyerr("could not navigate to sky jukebox. (is there a suitable save file?)");
    cleanup();
    return false;
  }

  return true;
}

int gettracknum() {
  // RAM locations for track num: 022A2090, 022A20B8 (both seem to represent the same value)
  return NDS::ARM9Read8(0x022A20B8);
}

std::string gettrackname() {
  // string starts at 022A29B1 and is 0-terminated, encoded as (presumably) ISO-8859-1
  u32 addr = (gettracknum() == 1) ? 0x022A29F1 : 0x022A29B1;
  u8 inchar;
  std::string outstr = "";

  while (inchar = NDS::ARM9Read8(addr)) {
    if (inchar < 128) outstr += inchar;
    else { // convert to UTF-8
      outstr += 0xC2 + (inchar > 0xBF);
      outstr += (inchar & 0x3F) + 0x80;
    }

    addr++;
  }

  return outstr;
}

bool trackchanged() {
  int newtracknum = gettracknum();
  if (prevtracknum != newtracknum) {
    prevtracknum = newtracknum;
    return true;
  }
  return false;
}

void marginfix(s16* audiobuf, int a, int& b) {
  int margin = 6;
  if (b < a - margin) {
    int last = b - 1;

    for (int i = b; i < a - margin; i++)
      ((u32*)audiobuf)[i] = ((u32*)audiobuf)[last];

    b = a - margin;
  }
}

bool skyaudio() {
  int len = 100; // we would like to read 100 samples

  // convert to NDS sample rate
  int numavail = SPU::GetOutputSize();
  if (numavail < ((len * 32823.6328125) / (float)SAMPLERATE)) return false;
  int numtoread = Frontend::AudioOut_GetNumSamples(len);

  // read samples
  s16 audiobuf_in[4096], audiobuf_out[4096];
  int numread = SPU::ReadOutput(audiobuf_in, numtoread);

  if (numread > 0) {
    // convert NDS audio data to output format
    marginfix(audiobuf_in, numtoread, numread);
    Frontend::AudioOut_Resample(audiobuf_in, numread, audiobuf_out, len, 256);

    // potentially write data to file
    if (writepcm) pcmfile.write(reinterpret_cast<char*>(&audiobuf_out), len * sizeof(s16) * 2);

    return true;
  } else return false; // no samples were available, try again next time
}

bool pcmreopen() {
  if (pcmfile) pcmfile.close();
  pcmfile = std::ofstream(outfolder + "/" + std::to_string(gettracknum()) + "-" + gettrackname() + ".pcm");

  if (!pcmfile) {
    skyerr("failed to open output file.");
    cleanup();
    return false;
  }
  return true;
}

bool checklast() {
  if (lasttrack) {
    while(skyaudio()) {}
    skyprint("done!");
    cleanup();
    return true;
  }
  return false;
}

int main(int argc, char const *argv[]) {
  skyprint("skyjuice");

  if (argc < 2) {
    skyerr("no rom path was specified.");
    return 1;
  }

  skyinit();

  if (!ROMManager::LoadROM(QStringList(argv[1]), true)) {
    skyerr("failed to load rom.");
    return 1;
  }

  if ((!std::filesystem::is_directory(outfolder)) && (!std::filesystem::create_directory(outfolder))) {
    skyerr("failed to create output folder.");
    return 1;
  }

  NDS::Start();
  skyprint("started nds!");

  while (true) {
    NDS::RunFrame(); // emulate
    NDS::SetKeyMask(0xFFF); // reset input

    if (gettracknum() == 0) {
      // we are not in the sky jukebox, try to navigate to it
      if (!navigate()) return 1;
    } else {
      if (trackchanged()) {
        // check whether we are done
        if (checklast()) return 0;

        // open the next file for writing
        writepcm = true;
        if (!pcmreopen()) return 1;

        std::cout << " ~new track: " << gettracknum() << " " << gettrackname() << "~" << std::endl;
        if (gettracknum() == 141) lasttrack = true;
      }
    }

    // Process buffered audio data
    while(skyaudio()) {}

    framecount++;
  }

  return 0;
}
